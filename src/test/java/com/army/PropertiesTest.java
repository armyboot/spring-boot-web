package com.army;


import com.army.domain.WebProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PropertiesTest {
    @Resource
    private WebProperties properties;

    @Test
    public void testProperties() throws Exception {
        System.out.println("title: " + properties.getTitle());
        System.out.println("description: " + properties.getDescription());
    }
}
