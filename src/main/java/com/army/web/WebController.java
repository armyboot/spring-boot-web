package com.army.web;

import com.army.domain.User;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class WebController {

    @PostMapping("/getUser")
    public User getUser() {
        User user = new User();
        user.setName("红狼");
        user.setAge(18);
        user.setPass("111111");
        return user;
    }

    @PostMapping("/getUsers")
    public List<User> getUsers() {
        List<User> users = new ArrayList<User>();
        User user1 = new User();
        user1.setName("Army");
        user1.setAge(36);
        user1.setPass("123456");
        users.add(user1);
        User user2 = new User();
        user2.setName("红狼");
        user2.setAge(18);
        user2.setPass("654321");
        users.add(user2);

        return users;
    }

    @GetMapping("/get/{name}")
    public User get(@PathVariable String name) {
        User user = new User();
        user.setName(name);

        return  user;
    }

    @PostMapping("/saveUser")
    public void saveUser(@Valid User user, BindingResult result) {
        System.out.println("user: " + user);
        if(result.hasErrors()) {
            List<ObjectError> list = result.getAllErrors();
            for (ObjectError error : list) {
                System.out.println(error.getCode() + "-" + error.getDefaultMessage());
            }
        }
    }
}
